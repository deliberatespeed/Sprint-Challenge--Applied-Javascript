Questions:

1. What is the DOM? The Document Object Model takes documents and converts them into nodes and objects so that programming languages can interface with the page. This way, you can use javascript to edit the document.
2. What is an event? An event is something that notifies the code that the user has done something or some automated process like page loading has been triggered. 
3. What is an event listener? This is something you can put into your code to listen for certain events. Like, you can tell your code to listen for a mouse click and then perform some action when that happens.
4. What is jQuery? This is a javascript library that appears to simplify code by making it shorter but actually makes it a lot more complicated and renders it virtually incomprehensible.
5. What is a component? A component is something intended to make it so that developers don't have to rewrite the functionality and styling for elements that are basically the same except for the content contained within. You can make a component and then just reuse it the next time you need to create a new similar element.